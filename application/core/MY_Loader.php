<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Loader class extends the core CI_Loader class.
 *
 * @author  Eric 'Aken' Roberts <eric@cryode.com>
 * @link    https://github.com/cryode/CodeIgniter_Smarty
 * @version 1.1.0
 * @license MIT
 */

class MY_Loader extends CI_Loader {

	/**
	 * Replace the default $this->load->view() method
	 * with our own, so we can use Smarty!
	 *
	 * This method works identically to CI's default method,
	 * in that you should pass parameters to it in the same way.
	 *
	 * @access	public
	 * @param	string	The template path name.
	 * @param	array	An array of data to convert to variables.
	 * @param	bool	Set to TRUE to return the loaded template as a string.
	 * @return	mixed	If $return is TRUE, returns string. If not, returns void.
	 */
	public function view($template, $data = array(), $return = FALSE)
	{
		// Get the CI super object, load related library.
		$CI =& get_instance();
		$CI->load->library('raintemplate');

		// totle process time
		global $BM;
		$elapsed = $BM->elapsed_time('total_execution_time_start', 'total_execution_time_end');
		$data['elapsed_time'] = $elapsed;
		
		// user info 
		$data['loginUser'] = $CI->ion_auth->user()->row();
		$data['isLogin'] = $data['loginUser'] ? TRUE : FALSE;
		
		// Assign any variables from the $data array.
		$CI->raintemplate->assign($data);
		
		// get output
		$output = $CI->raintemplate->draw($template, $return_string = true );

		// Return the output if the return value is TRUE.
		if ($return === TRUE)
		{
			return $output;
		}
		
	}
}


/* End of file Loader.php */
/* Location: ./application/core/Loader.php */
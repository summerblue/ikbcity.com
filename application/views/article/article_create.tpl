{include="editor/header"}

{include="editor/notify"}

{include="editor/loading"}

<div id="main" class="bye">
	
  {include="editor/nav"}
    
  <div id="app_wrap" class="container-fluid">
	  
	  <div class="operation-bar">
	  
		  <div id="filename">
		    <span contenteditable="true">foo</span>
		  </div>

		  
			<div class="book_create_submitbtn pull-right">
				
			  <button href="#" id="hide-topnav" class="btn btn-large"><i class="icon-ok"></i> 隐藏顶部导航</button>
			  <button href="#" id="show-topnav" class="btn btn-large"><i class="icon-ok"></i> 显示顶部导航</button>
		  
			  <button type="button" class="btn btn-primary btn-large" ><i class="icon-ok"></i> 发布 Book</button>
			  <button type="button" class="btn btn-primary btn-large" ><i class="icon-save"></i> 存为草稿</button>
		  </div>
		  
	  </div>	
  
    <div id="editor"></div>
	<div id="preview" class="shadowbox">
	 </div>
  </div> <!-- end #app_wrap -->
</div> <!-- end #main -->
<iframe id="downloader" class="hide"></iframe>
<div id="modal-generic" class="modal hide fade in">
  <div class="modal-header">
    <a href="#" class="close" data-dismiss="modal">&times;</a>
    <h3></h3>
  </div>
  <div class="modal-body">
  </div>
  <div class="modal-footer">
  </div>
</div>


{include="editor/footer"}

<script type="text/javascript">

$(document).ready(function(){
	
	editor = ace.edit("editor")
	
	$('#hide-topnav').click(function() {
		$("#top-nav").slideUp();
		$('#app_wrap').animate({top: 5}, 300, function() {editor.resize();});
	});
	$('#show-topnav').click(function() {
		$("#top-nav").slideDown();
		$('#app_wrap').animate({ top: 50}, 300, function() {editor.resize();});
	});
})
</script>
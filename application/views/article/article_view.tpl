{include="common/header"}

{include="common/topnav"}


<link rel="stylesheet" title="Rainbow" href="assets/css/highlight/rainbow.css">
<link rel="stylesheet" href="assets/css/editor/GitHub_Custom.css">

<div class="span7" id="mainbar">
	
	
	<div class="text-center" id="pagewaiting">
		<i class="icon-spinner icon-spin icon-4x"></i>
	</div>
	
	
	<div id="preview" class="shadowbox article-view hide">
#超级无敌牛叉编辑器

##MaHua是什么?
一个在线编辑markdown文档的编辑器

向Mac下优秀的markdown编辑器mou致敬

```python
@requires_authorization
def somefunc(param1='', param2=0):
    r'''A docstring'''
    if param1 &gt; param2: # interesting
        print 'Gre\'ater'
    return (param2 - param1 + 1) or None

class SomeClass:<br>    pass

&gt;&gt;&gt; message = '''interpreter
... prompt'''
```

##MaHua有哪些功能？
###MaHua有哪些功能？

1. 方便的`导入导出`功能
    *  直接把一个markdown的文本文件拖放到当前这个页面就可以了
    *  导出为一个html格式的文件，样式一点也不会丢失
* 编辑和预览`同步滚动`，所见即所得（右上角设置）
* `VIM快捷键`支持，方便vim党们快速的操作 （右上角设置）
* 强大的`自定义CSS`功能，方便定制自己的展示
* 有数量也有质量的`主题`,编辑器和预览区域
* 完美兼容`Github`的markdown语法
* 预览区域`代码高亮`
* 所有选项自动记忆

```sh
git clone [git-repo-url] dillinger
cd dillinger
npm i
mkdir -p public/files/{md,html}
node app
```

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

###有问题反馈
* 邮件(dev.hubo#gmail.com, 把#换成@)
* QQ: 287759234
* weibo: [@草依山](http://weibo.com/ihubo)
* twitter: [@ihubo](http://twitter.com/ihubo)

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##感激
感谢以下的项目,排名不分先后

* [mou](http://mouapp.com/) 
* [ace](http://ace.ajax.org/)
* [jquery](http://jquery.com)

##关于作者

var ihubo = {
    nickName  : "草依山",
    site : "http://jser.me"
}

#超级无敌牛叉编辑器

##MaHua是什么?
一个在线编辑markdown文档的编辑器

向Mac下优秀的markdown编辑器mou致敬

```python
@requires_authorization
def somefunc(param1='', param2=0):
    r'''A docstring'''
    if param1 &gt; param2: # interesting
        print 'Gre\'ater'
    return (param2 - param1 + 1) or None

class SomeClass:<br>    pass

&gt;&gt;&gt; message = '''interpreter
... prompt'''
```

##MaHua有哪些功能？

###MaHua有哪些功能？
####MaHua有哪些功能？
####MaHua有哪些功能？
1. 方便的`导入导出`功能
    *  直接把一个markdown的文本文件拖放到当前这个页面就可以了
    *  导出为一个html格式的文件，样式一点也不会丢失
* 编辑和预览`同步滚动`，所见即所得（右上角设置）
* `VIM快捷键`支持，方便vim党们快速的操作 （右上角设置）
* 强大的`自定义CSS`功能，方便定制自己的展示
* 有数量也有质量的`主题`,编辑器和预览区域
* 完美兼容`Github`的markdown语法
* 预览区域`代码高亮`
* 所有选项自动记忆

```sh
git clone [git-repo-url] dillinger
cd dillinger
npm i
mkdir -p public/files/{md,html}
node app
```

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(dev.hubo#gmail.com, 把#换成@)
* QQ: 287759234
* weibo: [@草依山](http://weibo.com/ihubo)
* twitter: [@ihubo](http://twitter.com/ihubo)

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##感激
感谢以下的项目,排名不分先后

* [mou](http://mouapp.com/) 
* [ace](http://ace.ajax.org/)
* [jquery](http://jquery.com)

##关于作者

var ihubo = {
    nickName  : "草依山",
    site : "http://jser.me"
}

#超级无敌牛叉编辑器

##MaHua是什么?
一个在线编辑markdown文档的编辑器

向Mac下优秀的markdown编辑器mou致敬

```python
@requires_authorization
def somefunc(param1='', param2=0):
    r'''A docstring'''
    if param1 &gt; param2: # interesting
        print 'Gre\'ater'
    return (param2 - param1 + 1) or None

class SomeClass:<br>    pass

&gt;&gt;&gt; message = '''interpreter
... prompt'''
```

##MaHua有哪些功能？

1. 方便的`导入导出`功能
    *  直接把一个markdown的文本文件拖放到当前这个页面就可以了
    *  导出为一个html格式的文件，样式一点也不会丢失
* 编辑和预览`同步滚动`，所见即所得（右上角设置）
* `VIM快捷键`支持，方便vim党们快速的操作 （右上角设置）
* 强大的`自定义CSS`功能，方便定制自己的展示
* 有数量也有质量的`主题`,编辑器和预览区域
* 完美兼容`Github`的markdown语法
* 预览区域`代码高亮`
* 所有选项自动记忆

```sh
git clone [git-repo-url] dillinger
cd dillinger
npm i
mkdir -p public/files/{md,html}
node app
```

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(dev.hubo#gmail.com, 把#换成@)
* QQ: 287759234
* weibo: [@草依山](http://weibo.com/ihubo)
* twitter: [@ihubo](http://twitter.com/ihubo)

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##感激
感谢以下的项目,排名不分先后

* [mou](http://mouapp.com/) 
* [ace](http://ace.ajax.org/)
* [jquery](http://jquery.com)

##关于作者

    var ihubo = {
        nickName  : "草依山",
        site : "http://jser.me"
    }



	</div>
	
</div><!--End of mainbar -->

<div class="span2" id="sidebar">
	<div id="toc"></div><!--/.well -->
</div>

{include="common/footer"}


<style type="text/css">
	#preview pre {
		width: 737px;
		margin-left: -30px;	
		border:none;
		padding:0;
	}
	
	#preview pre code {
		padding: 10px;
		font-size: 12px;
		border: none;
		border-radius: 6px;
	}
	
	
	#toc {
		top: 0px;
		left: 40px;
		position: relative;
		box-shadow: inset -5px 0 5px 0px #f8f8f8;
		width: 203px;
		padding-top: 20px;
		color: #53A3B5;
		background-color: #eeeeee;
		border-radius: 11px;
	}

	#sidebar .fix-position {
		left: 50%;
		position: fixed;
		margin-left: 270px;
	}
	
	#toc ul {
	    margin: 0;
	    padding: 0;
	    list-style: none;
	}

	#toc li {
	    padding: 5px 10px;
	}

	#toc a {
	    color: #53A3B5;
	    text-decoration: none;
	    display: block;
	}

	#toc .toc-h2 {
	    padding-left: 10px;
	}

	#toc .toc-h3 {
	    padding-left: 20px;
	}
	
	#toc .toc-h4 {
	    padding-left: 30px;
	}

	#toc .toc-active {
	    background: #336699;
	    box-shadow: inset -5px 0px 10px -5px #000;
	}
</style>

<script src="assets/js/marked.js" type="text/javascript"></script>
<script src="assets/js/highlight.pack.js" type="text/javascript"></script>
<script src="assets/js/toc.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {
	hljs.initHighlightingOnLoad();

	$("#preview").html( marked($("#preview").text()) );
	// parse the markup, rander page 
	$('pre code').each(function(i, e) {hljs.highlightBlock(e)});
	
	$("#pagewaiting").hide();
	$("#preview").fadeIn(500);
	
	// table of content
	$('#toc').toc({
	    'selectors': 'h2,h3,h4', //elements to use as headings
	    'container': '#preview', //element to find all selectors in
	    'smoothScrolling': true, //enable or disable smooth scrolling on click
	    'prefix': 'toc', //prefix for anchor tags and class names
	    'onHighlight': function(el) {}, //called when a new section is highlighted 
	    'highlightOnScroll': true, //add class to heading that is currently in focus
	    'highlightOffset': 200, //offset to trigger the next headline
	    'anchorName': function(i, heading, prefix) { //custom function for anchor name
	        return prefix+i;
	    },
	    'headerText': function(i, heading, $heading) { //custom function building the header-item text
	        return $heading.text();
	    },
		'itemClass': function(i, heading, $heading, prefix) { // custom function for item class
	  		return prefix + '-' + $heading[0].tagName.toLowerCase();
		}
	});
	
});
	
</script>
{include="common/header"}

{include="common/topnav"}


<div class="span5" id="" style="margin-left: 15px;">
	
    <div id="app_wrap" class="book_create_main">
        <div id="filename" >
			<h2><span contenteditable="true" class="book_create_h2">foo</span></h2>
        </div>
      <div id="editor" class="shadowbox"></div>
      
	  <div class="clearfix"></div>
	  <div class="book_create_submitbtn text-center">
		  <button type="button" class="btn btn-primary btn-large" ><i class="icon-ok"></i> 发布 Book</button>
		  <button type="button" class="btn btn-primary btn-large" ><i class="icon-save"></i> 存为草稿</button>
	  </div>
      
	  
    </div> <!-- end #app_wrap -->
	
</div><!--End of mainbar -->

<div class="span3">
	<div id="preview" class="shadowbox"></div>
</div>

{include="common/footer"}


<script src="assets/js/htmlparser.js" type="text/javascript"></script>
<script src="assets/js/html2json.js" type="text/javascript"></script>

<script src="assets/js/xml2json.js" type="text/javascript"></script>

<script src="assets/js/editor/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/js/editor/mode-markdown.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/js/editor/showdown.js" type="text/javascript"></script>
<script src="assets/js/editor/keymaster.js" type="text/javascript"></script>

<script src="assets/js/marked.js" type="text/javascript"></script>
<script src="assets/js/highlight.pack.js" type="text/javascript"></script>

<script src="assets/js/book.js" type="text/javascript"></script>

<script type="text/javascript">
	hljs.initHighlightingOnLoad();
	
	$("#preview").html(xml2json.parser($("#preview").html(),'b,i','html'));
</script>


<style type="text/css">
#editor {
	position: relative;
    width: 500px;
    height: 600px;
	margin-bottom: 30px;
}
#preview {
	margin-bottom: 90px;
	width: 380px;
position: relative;
margin-left: 30px;
padding: 20px;
left: 0%;
right: 10px;
  top: 50px;
  overflow: auto;
  background: rgba(255,255,255,0.9);
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-box-flex: 1;
  -moz-box-flex: 1;
  -ms-box-flex: 1;
  -webkit-box-flex: 1;
  -moz-box-flex: 1;
  -ms-box-flex: 1;
  -o-box-flex: 1;
  box-flex: 1;
}
.shadowbox {
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  -webkit-box-shadow: 0px 0px 80px rgba(0,0,0,0.3), inset 0 0 5px rgba(0,0,0,0.6);
  margin: 0 auto;
}
</style>
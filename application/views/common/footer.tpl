	<div class="clearfix"></div>

	<footer class="footer">
		<div class="pull-left">
            <p>&copy; EST 2012 - 2013</p>
			<P><i class="icon-heart-empty"></i> Lovingly made by CJ & Luna</P>					
		</div>
		<div class="pull-right">
			<p><i class="icon-bolt"></i> Page rendered in {$elapsed_time} seconds</p>
		</div>
    </footer>

	{include="common/modules"}
		
    </div> <!-- /container -->
	
	{include="common/scripts"}
	
    </body>
</html>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>iKBCity: Knowledge worth spreading</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

		<base href="http://ikbcity.com/" />
        <link rel="stylesheet" href="assets/css/bootstrap.css">
		<link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/icon-animated.css">
		
		<link rel="stylesheet" href="assets/css/bootstrap-modal.css">
		
		{include="common/icon"}
		
    </head>
    <body>
        <div class="container" id="container">

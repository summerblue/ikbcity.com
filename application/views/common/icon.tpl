<link rel="shortcut icon" href="favicon.ico">

<link rel="apple-touch-icon" href="assets/icon/apple-touch-icon-57x57-precomposed.png" />
<link rel="apple-touch-icon" sizes="72x72" href="assets/icon/apple-touch-icon-72x72-precomposed.png" />
<link rel="apple-touch-icon" sizes="114x114" href="assets/icon/apple-touch-icon-114x114-precomposed.png" />
<link rel="apple-touch-icon" sizes="144x144" href="assets/icon/apple-touch-icon-144x144-precomposed.png" />
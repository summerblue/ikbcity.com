
<div id="loginModal" class="modal hide fade">
	
    <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3><i class="icon-coffee green"></i> 请填写口令登录</h3>
    </div>
	
	<div class="modal-body clearfix">
		<div class="widget-main" style="margin-top: 15px;">
			<form id="login-form" class="span3 pull-left" style="margin-bottom: 10px;" action="/user/login" method="post" accept-charset="utf-8">
				<div class="input-append input-block-level">
				  <input type="email" name="identity" class="input-block-level" id="appendedInput" placeholder="请输入邮箱/用户名" tabindex="101">
				  <span class="add-on"><i class="icon-envelope"></i></span>
				</div>
				
				<div class="input-append input-block-level" >
				  <input type="password" name="password" class="input-block-level" id="appendedInput" placeholder="请输入密码" tabindex="102">
				  <span class="add-on"><i class="icon-lock"></i></span>
				</div>
				<label class="checkbox inline pull-right"><input type="checkbox" name="remember" id="remember" value="1" tabindex="103"> 记住我的登录状态</label>
				<input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;" hidefocus="true" tabindex="-1"/>
				
			</form>
		</div>
		
		<!--/widget-main-->
		<div class="toolbar span2">
			<div class="pull-right">
				<a href="#" onClick="$('#loginModal').modal('hide'); $('#forgetModal').modal('show'); return false;" >
					忘记密码/找回密码 <i class="icon-arrow-right"></i>
				</a>
			</div>
			<div class="pull-right">
				<a href="#" onClick="$('#loginModal').modal('hide'); $('#registerModal').modal('show'); return false;" >
					加入我们/注册 <i class="icon-arrow-right"></i>
				</a>
			</div>
		</div>
	
	</div>
	
    <div class="modal-footer clearfix">
      <button type="button" data-dismiss="modal" class="btn" tabindex="104">关闭</button>
      <button type="button" class="btn btn-primary" onClick="$('#login-form').submit();" tabindex="105"><i class="icon-key"></i> 登录</button>
    </div>
</div>


<div id="forgetModal" class="modal hide fade">
    <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3><i class="icon-key"></i> 找回密码</h3>
    </div>
	<div class="modal-body clearfix">
		<div class="widget-main" style="margin-top: 15px;">
			<form class="span3 pull-left" style="margin-bottom: 10px;">
				<p>发送找回密码的邮件到注册邮箱:</p>
				<div class="input-append input-block-level">
					 <input type="email" class="input-block-level" id="appendedInput" placeholder="请提供您的注册邮箱">
					 <span class="add-on"><i class="icon-envelope"></i></span>
				</div>
			</form>
		</div>
		<div class="toolbar span2">
			<div class="pull-right">
				<a href="#" onclick="$('#forgetModal').modal('hide'); $('#forgetModal').modal('show'); return false;">
					返回登录 
					<i class="icon-arrow-right"></i>
				</a>
			</div>
			<div class="pull-right">
				<a href="#" onClick="$('#forgetModal').modal('hide'); $('#registerModal').modal('show'); return false;" >
					加入我们/注册 <i class="icon-arrow-right"></i>
				</a>
			</div>
		</div>
	</div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn">关闭</button>
      <button type="button" class="btn btn-primary"><i class="icon-lightbulb"></i> 发送</button>
    </div>
</div><!--/forgot-box-->




<div id="registerModal" class="modal hide fade">
	
    <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3><i class="icon-group blue"></i> 新用户注册</h3>
    </div>
	
	<div class="modal-body clearfix" style="">
		<div class="widget-main" style="margin-top: 15px;">
			<form action="/user/register" method="post" accept-charset="utf-8" id="register-form" class="span3 pull-left" style="margin-bottom: 10px;">
		
				
				<div class="input-append input-block-level">
				  <input type="email" name="email" class="input-block-level" id="email" placeholder="请填写邮箱">
				  <span class="add-on"><i class="icon-envelope"></i></span>
				</div>
				
				<div class="input-append input-block-level" >
				  <input type="text" class="input-block-level" name="username" id="" placeholder="用户名">
				  <span class="add-on"><i class="icon-user"></i></span>
				</div>
				
				<div class="input-append input-block-level" >
				  <input type="password" class="input-block-level" name="password" id="" placeholder="请输入密码">
				  <span class="add-on"><i class="icon-lock"></i></span>
				</div>
				
				<div class="input-append input-block-level" >
				  <input type="password" class="input-block-level" name="password_confirm" id="" placeholder="重新输入一遍密码">
				  <span class="add-on"><i class="icon-retweet"></i></span>
				</div>
				
				<label class="checkbox inline pull-right"><input type="checkbox" id="inlineCheckbox1" value="option1"> 我同意用户协议</label>
				<div class="clearfix"></div>
			    <div class="text-center register-submit">
			      <button type="reset" class="btn btn-large"><i class="icon-refresh"></i> 重置</button>
			      <button type="button" class="btn disable btn-large" onClick="$('#register-form').submit();">注册 <i class="icon-arrow-right icon-on-right"></i></button>
			    </div>
			</form>
		</div>
		
		<!--/widget-main-->
		<div class="toolbar span2">
			<div class="pull-right">
				<a href="#" onClick="$('#registerModal').modal('hide'); $('#forgetModal').modal('show'); return false;">
					忘记密码/找回密码
					 <i class="icon-arrow-right"></i>
				 </a>
			</div>
			<div class="pull-right">
				<a href="#" onClick="$('#registerModal').modal('hide'); $('#loginModal').modal('show'); return false;" >
					返回登录 <i class="icon-arrow-right"></i>
				</a>
			</div>
		</div>
	
	</div>
</div><!--/register-box-->




<!-- /Back to top -->
<span class="backToTop hide text-center" style="display: block;">
	<a href="#"><i class="icon-angle-up icon-3x"></i></a>
</span>

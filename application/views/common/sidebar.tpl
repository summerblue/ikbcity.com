

<div class="span2" id="sidebar">
    <div class="span2">
		
		<a href="" class="btn text-center banner" >2013.5.3</a>
		
    </div>
    <div class="span2 box">
		
		<ul class="unstyled nav-group" id="sidebar-nav">
			<li><a  href="/draft"><i class="icon-leaf"></i> 我的草稿 <span class="badge badge-info">8</span></a></li>
			<li><a href="/question/following"><i class="icon-file-alt"></i> 我的文章 </a></li>
			<li><a href="/question/following"><i class="icon-book"></i> 我的 Books </a></li>
			<li><a href="/collections"><i class="icon-bookmark"></i> 我的收藏 </a></li>
		</ul>
        
   </div>
   
    <div class="span2 box">
        <h2><i class="icon-tags"></i>我的标签</h2>
        
		<div id="myTags">
			<a href="/tag/php" class="btn lable user-tag" title="show questions tagged 'php'" rel="tag">php</a>
			<a href="/tag/ios" class="btn lable user-tag" title="show questions tagged 'ios'" rel="tag">ios</a>
			<a href="/tag/objective-c" class="btn lable user-tag" title="show questions tagged 'objective-c'" rel="tag">objective-c</a>
			<a href="/tag/mobile" class="btn lable user-tag" title="show questions tagged 'mobile'" rel="tag">mobile</a>
			<a href="/tag/jquery" class="btn lable user-tag" title="show questions tagged 'jquery'" rel="tag">jquery</a>
			<a href="/tag/linux" class="btn lable user-tag" title="show questions tagged 'linux'" rel="tag">linux</a>
			<a href="/tag/machine-learning" class="btn lable user-tag" title="show questions tagged 'machine-learning'" rel="tag">machine-learning</a>
			<a href="/tag/mac" class="btn lable user-tag" title="show questions tagged 'mac'" rel="tag">mac</a>
			<a href="/tag/xcode" class="btn lable user-tag" title="show questions tagged 'xcode'" rel="tag">xcode</a>
			<a href="/tag/osx" class="btn lable user-tag" title="show questions tagged 'osx'" rel="tag">osx</a>
			<a href="/tag/javascript" class="btn lable user-tag" title="show questions tagged 'javascript'" rel="tag">javascript</a>
		</div>
		
    </div>
	
	
</div><!--End of sidebar -->

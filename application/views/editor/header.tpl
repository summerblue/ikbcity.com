<!DOCTYPE html>
<html lang="en">
<head>
  <title>{$title}</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="{$description}">
  <meta name="author" content="Joe McCann">
  
  
  <base href="http://ikbcity.com/" />
  
  <link href="assets/css/editor/style.css" rel="stylesheet">
  <link href="assets/css/editor/GitHub_Custom.css" rel="stylesheet">
  
  <link rel="stylesheet" title="Rainbow" href="assets/css/highlight/rainbow.css">
	
  <!-- For IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

	
	{include="common/icon"}
	
</head>

<body>
  <noscript>
    <p>Oooooo, sorry that's the wrong answer. You actually need JavaScript to use Dillinger.</p>
    <p>See, Dillinger is an application, not a web<em>site</em>. JavaScript is a requirement.</p>
    <p>Be sure and come back when you have JavaScript enabled - It's 2012!!!!111!1!</p>
  </noscript>

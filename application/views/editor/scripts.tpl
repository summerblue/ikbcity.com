<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/editor/bootstrap.min.js"></script>
<script src="assets/js/editor/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/js/editor/mode-markdown.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/js/editor/showdown.js" type="text/javascript"></script>
<script src="assets/js/editor/keymaster.js" type="text/javascript"></script>

<script src="assets/js/marked.js" type="text/javascript"></script>
<script src="assets/js/highlight.pack.js" type="text/javascript"></script>

<script src="assets/js/editor/dillinger.js" type="text/javascript"></script>

<script type="text/javascript">
	hljs.initHighlightingOnLoad();
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-??????-15']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
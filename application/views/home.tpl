{include="common/header"}

{include="common/topnav"}

<div class="span7" id="mainbar">
	
	<ul class="nav nav-tabs" id="myTab">
	  <li class="active"><a href="#article-list-new" data-toggle="tab">最新文章</a></li>
	  <li><a href="#article-list-hot" data-toggle="tab">最热文章</a></li>
	  <li><a href="#book-list-new" data-toggle="tab">最新 Books</a></li>
	  <li><a href="#book-list-hot" data-toggle="tab">最热 Books</a></li>
	</ul>

	<div class="tab-content">
		
	  <div class="tab-pane fade active in" id="article-list-new">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div> <!--End of article-list-new-->
	  
	  
	  <div class="tab-pane fade" id="article-list-hot">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div>
	  
	  <div class="tab-pane fade" id="book-list-new">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
		  
	  </div>
	  
	  <div class="tab-pane fade" id="book-list-hot">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div>
	</div>

  
</div><!--End of mainbar -->


{include="common/sidebar"}


{include="common/footer"}


{include="common/header"}

<script src="assets/js/holder.js" type="text/javascript"></script>

{include="common/topnav"}

<div class="span7" id="mainbar">
	
	
	<div class="well tag-description">
		<ul class="thumbnails">
        <li class="span2">
          <a href="#" class="thumbnail">
            <img data-src="holder.js/300x160" alt="160x160" style="width: 160px; height: 160px;" src=""> 
			<!--  <img data-src="" alt="160x160" style="width: 160px; height: 160px;" src="http://itekblog.com/wp-content/uploads/2012/08/python-logo.png">-->
          </a>
        </li>
        <li class="span4">
			<h2>Python</h2>
			<p>Python（英语发音：/ˈpʌɪθ(ə)n/），是一种面向对象、直译式电脑编程语言，也是一种功能强大的通用型语言，已经具有近二十年的发展历史，成熟且稳定。它包含了一组完善而且容易理解的标准库，能够轻松完成很多常见...</p>
        </li>
      </ul>
	</div>
	
	
	<ul class="nav nav-tabs" id="myTab">
	  <li class="active"><a href="#article-list-new" data-toggle="tab">最新文章</a></li>
	  <li><a href="#article-list-hot" data-toggle="tab">最多投票</a></li>
	  <li><a href="#book-list-new" data-toggle="tab">最新 Books</a></li>
	  <li><a href="#book-list-hot" data-toggle="tab">Books 投票</a></li>
	  
	  
	  
	  <form class="pull-right navbar-search input-append searchform" id="" action="" style="margin-top: 3px;"> 
       	<input type="text" class="input-large" id="appendedInput" placeholder="标签下搜索">
	 	<span class="add-on btn"><i class="icon-search icon-large" onclick="console.log(234);"></i></span>
      </form>
	</ul>

	<div class="tab-content">
		
	  <div class="tab-pane fade active in" id="article-list-new">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div> <!--End of article-list-new-->
	  
	  
	  <div class="tab-pane fade" id="article-list-hot">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div>
	  
	  <div class="tab-pane fade" id="book-list-new">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
		  
	  </div>
	  
	  <div class="tab-pane fade" id="book-list-hot">
		  <div id="content">
			  {include="article/article_list"}
		  </div>
	  </div>
	</div>

  
</div><!--End of mainbar -->


{include="common/sidebar"}


{include="common/footer"}

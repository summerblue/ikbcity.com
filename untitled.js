

$('#sidebar').css('position':'relative');

sidetop = $('#sidebar').offset().top;
// toggle fixed position of the side bar when the document scrolls
var $win = $( window ).bind("scroll",function(){
	var top = $( document ).scrollTop();
	$('#sidebar').stop( true, true ).animate({
		"top":  top < sidetop ? 0 : top - sidetop + 8 
	}, 200 );
});

